// ----------------------------------
// projects/c++/allocator/Allocator.h
// Copyright (C) 2019
// Glenn P. Downing
// ----------------------------------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cmath>     // abs
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <iostream>
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * confirms heap contains no mistakes
     */
    bool valid () const {
        iterator it = iterator((int*)(&a[0]));
        iterator end = iterator((int*)((char*)&a + N));
        bool prev_open = *it > 0;
        int prev_sentinel = *it;
        ++it;
        while(it != end) {
            // check for adjacent free blocks
            if(*it > 0) {
                if(prev_open)
                    return false;
                prev_open = true;
            }
            else
                prev_open = false;

            // check sentinel accuracy
            if(*(&(*it)-1) != prev_sentinel)
                return false;
            prev_sentinel = *it;
            ++it;
        }
        int last_sentinel = *(int*)((char*)&a + N - 4);
        if(last_sentinel != prev_sentinel)
            return false;
        return true;
    }

public:
    // --------
    // iterator
    // --------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return (lhs._p == rhs._p);
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) : _p(p) {}

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            int val = abs(*_p);
            _p = (int*)((char*)_p + val + 8);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }
    };

    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        // assign sentinels to empty block
        int* first = (int*)(&a[0]);
        *first = N-8;
        int* second = (int*)(&a[N-4]);
        *second = N-8;
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type n) {
        // linear search blocks (using iterator) for first qualifying free block
        iterator it = iterator((int*)(&a[0]));
        int new_sentinel_value = -n;
        bool fill_rest = false;
        iterator end = iterator((int*)((char*)&a + N));
        // check for enough room for another block
        // if not give remaining space
        while(*it < 0 || ((*it >= 0) && (*it < (int)n))) {
            it++;
            if(it == end)
                throw std::bad_alloc();
        }
        if(*it == (int)n)
            fill_rest = true;
        else if((*it-n) < (sizeof(T)+8)) {
            fill_rest = true;
            new_sentinel_value = -(*it);
        }
        // add used block sentinel
        int *p = &(*it);
        int old_sentinel_value = *it-(abs(new_sentinel_value) + 8);
        *p = new_sentinel_value;
        T* result = (T*)(p + 1);
        char *ptr = (char*)p;
        ptr += (4 + -(new_sentinel_value));
        p = (int*)ptr;
        *p = new_sentinel_value;
        p += 1;
        // update free block sentinel, not needed if fill_rest
        if(!fill_rest) {
            *p = old_sentinel_value;
            ptr = (char*)p;
            ptr += old_sentinel_value+4;
            p = (int*)ptr;
            *p = old_sentinel_value;
        }
        assert(valid());
        return result;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);
        assert(valid());
    }

    // ----------
    // check_left
    // ----------

    /**
     * checks left side for coalescing free blocks
     */
    int* check_left(int* start, int* end) {
        int left_val = *(start-1);
        if(left_val > 0) {
            int new_val = left_val + *start + 8;
            char* ptr = (char*) start;
            ptr -= left_val + 8;
            start = (int*)ptr;
            *start = new_val;
            *end = new_val;
        }
        return start;
    }

    // ----------
    // check_right
    // ----------

    /**
     * checks right side for coalescing free blocks
     */
    void check_right(int* start, int* end) {
        int right_val = *(end+1);
        if(right_val > 0) {
            int new_val = right_val + *end + 8;
            char* ptr = (char*) end;
            ptr += right_val + 8;
            end = (int*)ptr;
            *start = new_val;
            *end = new_val;
        }

    }

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     */
    void deallocate (pointer p, size_type n) {
        // update used block sentinel to free block sentinel (- -> +)
        // coalesce adjacent free blocks
        int* start_p = ((int*)p)-1;
        int sentinel_value = -(*start_p);
        *start_p = sentinel_value;
        char *ptr = (char*)start_p;
        ptr += sentinel_value+4;
        int* end_p = (int*)ptr;
        *end_p = sentinel_value;
        if(start_p != (int*)(&a[0]))
            start_p = check_left(start_p, end_p);
        if(end_p+1 != (int*)(&a[0] + N))
            check_right(start_p, end_p);
        ++n;
        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();
        assert(valid());
    }

    // -----
    // print
    // -----

    /**
     * prints the blocks in the heap
     */
    void print () {
        iterator it = iterator((int*)(&a[0]));
        iterator end = iterator((int*)((char*)&a + N));
        std::cout << *it;
        ++it;
        while(it != end) {
            std::cout << " " << *it;
            ++it;
        }
    }

    // ---------
    // find_busy
    // ---------

    /**
     * finds the nth busy block in the heap
     */
    pointer find_busy(int n) {
        iterator it = iterator((int*)(&a[0]));
        iterator end = iterator((int*)((char*)&a + N));
        int count = 0;
        while(it != end) {
            if(*it < 0) {
                ++count;
                if(count == n)
                    return (pointer)((&(*it))+1);
            }
            ++it;
        }
        return nullptr;
    }

    // -----
    // input
    // -----

    /**
     * parses the input
     */
    void input(int num);

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }
};

#endif // Allocator_h
