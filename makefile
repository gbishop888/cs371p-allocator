.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules

FILES :=                                           \
	.gitignore                                     \
	html                                           \
	allocator-tests                                \
	allocator-tests/gbishop888-RunAllocator.in     \
	allocator-tests/gbishop888-RunAllocator.out    \
	Allocator.h                                    \
	Allocator.log                                  \
	makefile                                       \
	RunAllocator.c++                               \
	RunAllocator.in                                \
	RunAllocator.out                               \
	TestAllocator.c++                              \

html: Doxyfile Allocator.h
	doxygen Doxyfile

allocator-tests:
	git clone https://gitlab.com/gbishop888/cs371p-allocator-tests.git allocator-tests

Allocator.log:
	git log > Allocator.log

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATEIC to YES
Doxyfile:
	doxygen -g

RunAllocator: Allocator.h Allocator.c++ RunAllocator.c++
	-cppcheck Allocator.h --language=c++
	-cppcheck Allocator.c++
	-cppcheck RunAllocator.c++
	g++ -pedantic -std=c++14 -Wall -Weffc++ -Wextra Allocator.c++ RunAllocator.c++ -o RunAllocator

RunAllocator.c++x: RunAllocator
	./RunAllocator < RunAllocator.in > RunAllocator.tmp
	-diff RunAllocator.tmp RunAllocator.out

TestAllocator: Allocator.h TestAllocator.c++
	-cppcheck Allocator.h --language=c++
	-cppcheck TestAllocator.c++
	g++ -fprofile-arcs -ftest-coverage -pedantic -std=c++14 -Wall -Weffc++ -Wextra  Allocator.h Allocator.c++ TestAllocator.c++ -o TestAllocator -lgtest -lgtest_main -pthread

TestAllocator.c++x: TestAllocator
	valgrind ./TestAllocator
	gcov -b Allocator.c++ | grep -A 5 "File '.*Allocator.h'"

all: RunAllocator TestAllocator

check: $(FILES)

clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.plist
	rm -f *.tmp
	rm -f RunAllocator
	rm -f TestAllocator

ctd:
	checktestdata TestAllocator.ctd RunAllocator.in

format:
	astyle Allocator.h
	astyle RunAllocator.c++
	astyle TestAllocator.c++

run: RunAllocator.c++x TestAllocator.c++x

versions:
	which         astyle
	astyle        --version
	@echo
	dpkg -s       libboost-dev | grep 'Version'
	@echo
	ls -al        /usr/lib/*.a
	@echo
	which         checktestdata
	checktestdata --version
	@echo
	which         cmake
	cmake         --version
	@echo
	which         cppcheck
	cppcheck      --version
	@echo
	which         doxygen
	doxygen       --version
	@echo
	which         g++
	g++           --version
	@echo
	which         gcov
	gcov          --version
	@echo
	which         git
	git           --version
	@echo
	which         make
	make          --version
	@echo
	which         valgrind
	valgrind      --version
	@echo
	which         vim
	vim           --version