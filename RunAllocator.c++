#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <sstream>
#include <algorithm>
#include "Allocator.h"
using namespace std;

void readInput(istream& r) {
    string temp;
    getline(r, temp);
    istringstream parse(temp);
    int numTest;
    parse >> numTest;
    getline(r, temp);
    for (int i = 0; i < numTest; i++) {
        my_allocator<double, 1000> alloc;
        getline(r, temp);
        do {
            istringstream parse(temp);
            int num;
            parse >> num;
            alloc.input(num);
            if (!r.good()) {
                break;
            }
            getline(r, temp);
        } while (!temp.empty());
        alloc.print();
        cout << endl;
    }
}

int main() {
    readInput(cin);
    return 0;
}