// ----------------------------------------
// projects/c++/allocator/TestAllocator.c++
// Copyright (C) 2019
// Glenn P. Downing
// ----------------------------------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.h"

TEST(TestAllocator, constructor1) {
    my_allocator<double, 1000> x;
    ASSERT_EQ(x[0], 992);
    ASSERT_EQ(x[996], 992);
}

TEST(TestAllocator, constructor2) {
    my_allocator<int, 1000> x;
    ASSERT_EQ(x[0], 992);
    ASSERT_EQ(x[996], 992);
}

TEST(TestAllocator, allocate1) {
    my_allocator<double, 1000> x;
    const int s = 40;
    x.allocate(s);
    const int i = x[0];
    ASSERT_EQ(i, -40);
    ASSERT_EQ(x[44], -40);
    ASSERT_EQ(x[48], 944);
    ASSERT_EQ(x[996], 944);
}

TEST(TestAllocator, allocate2) {
    my_allocator<double, 1000> x;
    const int s = 992;
    x.allocate(s);
    ASSERT_EQ(x[0], -992);
    ASSERT_EQ(x[996], -992);
}

TEST(TestAllocator, allocate3) {
    my_allocator<double, 1000> x;
    const int s = 984;
    x.allocate(s);
    ASSERT_EQ(x[0], -992);
    ASSERT_EQ(x[996], -992);
}

TEST(TestAllocator, deallocate1) {
    my_allocator<double, 1000> x;
    const int s = 992;
    double *p = x.allocate(s);
    ASSERT_EQ(x[0], -992);
    ASSERT_EQ(x[996], -992);
    x.deallocate(p,1);
    ASSERT_EQ(x[0], 992);
    ASSERT_EQ(x[996], 992);
}

TEST(TestAllocator, deallocate2) {
    my_allocator<double, 1000> x;
    const int s = 40;
    x.allocate(s);
    double *p = x.allocate(s);
    ASSERT_EQ(x[0], -40);
    ASSERT_EQ(x[44], -40);
    ASSERT_EQ(x[48], -40);
    ASSERT_EQ(x[92], -40);
    ASSERT_EQ(x[96], 896);
    ASSERT_EQ(x[996], 896);
    x.deallocate(p,1);
    ASSERT_EQ(x[48], 944);
    ASSERT_EQ(x[996], 944);
}

TEST(TestAllocator, deallocate3) {
    my_allocator<double, 1000> x;
    const int s = 40;
    double *p = x.allocate(s);
    double *p2 = x.allocate(s);
    x.allocate(s);
    ASSERT_EQ(x[0], -40);
    ASSERT_EQ(x[44], -40);
    ASSERT_EQ(x[48], -40);
    ASSERT_EQ(x[92], -40);
    ASSERT_EQ(x[96], -40);
    ASSERT_EQ(x[140], -40);
    ASSERT_EQ(x[144], 848);
    ASSERT_EQ(x[996], 848);
    x.deallocate(p,1);
    x.deallocate(p2,1);
    ASSERT_EQ(x[0], 88);
    ASSERT_EQ(x[92], 88);
    ASSERT_EQ(x[96], -40);
    ASSERT_EQ(x[140], -40);
    ASSERT_EQ(x[144], 848);
    ASSERT_EQ(x[996], 848);
}

TEST(TestAllocator, deallocate4) {
    my_allocator<double, 1000> x;
    const int s = 40;
    double *p = x.allocate(s);
    double *p2 = x.allocate(s);
    ASSERT_EQ(x[0], -40);
    ASSERT_EQ(x[44], -40);
    ASSERT_EQ(x[48], -40);
    ASSERT_EQ(x[92], -40);
    ASSERT_EQ(x[96], 896);
    ASSERT_EQ(x[996], 896);
    x.deallocate(p,1);
    ASSERT_EQ(x[0], 40);
    ASSERT_EQ(x[44], 40);
    x.deallocate(p2,1);
    ASSERT_EQ(x[0], 992);
    ASSERT_EQ(x[996], 992);
}

TEST(TestAllocator, constructanddestroy1) {
    my_allocator<double, 1000> x;
    const int s = 40;
    const double v = 8;
    double *p = x.allocate(s);
    x.construct(p, v);
    ASSERT_EQ(v, *p);
    x.destroy(p);
}