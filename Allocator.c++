#include "Allocator.h"

template <>
void my_allocator<double, 1000>::input(int num) {
    if (num > 0)
        my_allocator<double, 1000>::allocate(num*sizeof(double));
    else
        my_allocator<double, 1000>::deallocate(my_allocator<double, 1000>::find_busy(abs(num)), 1);
}